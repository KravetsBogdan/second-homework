// 1) Типи даних у JS: Number, String, Bolean, Null, Undefined, Symbol, typeof, bigInteger;
// 2) == не сроге порівняння; повертає true якщо значення однакові;
//   === строге порівняння; повертає true якщо значення і тип даних однаковий;
// 3) Оператор це математичний знак який виконує дії з операндами;Наприклад 2 + 3, де 2 і 3 це операнди а + це оператор;
// 10 / 2, де 10 і 2 це операнди а / це оперетор;

let userName;
while (!userName || !isNaN(userName)) {
    userName = prompt('Enter your name').trim();
}
console.log(userName);

let age;
while (!age || isNaN(age)) {
    age = +prompt('Enter your age').trim();
}
console.log(age);

if (age < 18) {
    alert('You are not allowed to visit this website')
} else if (age >= 18 && age <= 22) {
    const request = confirm('Are you sure you want to continue?')
    if (request === true) {
        alert('Welcome, ' + userName)
    } else {
        alert('You are not allowed to visit this website')
    }
} else {
    alert(' Welcome ' + userName)
}